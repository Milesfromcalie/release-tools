package labels

const (
	SUCCESS = "success"
	FAILED  = "failed"
)

func FromValues(name string, values []string) *stringSlice {
	return &stringSlice{
		base:   &base{name: name},
		values: values,
	}
}

func SuccessOrFailed(name string) *stringSlice {
	return FromValues(name, []string{SUCCESS, FAILED})
}

func Environment(env string) *stringSlice {
	return FromValues(env, []string{"gstg", "gprd", "pre", "gstg-ref", "ops", "testbed", "db-benchmarking"})
}

func Stage(env string) *stringSlice {
	return FromValues(env, []string{"main", "cny"})
}

type stringSlice struct {
	values []string

	*base
}

func (s *stringSlice) Values() []string {
	return s.values
}

func (s *stringSlice) CheckValue(value string) bool {
	for _, allowedValue := range s.values {
		if value == allowedValue {
			return true
		}
	}

	return false
}

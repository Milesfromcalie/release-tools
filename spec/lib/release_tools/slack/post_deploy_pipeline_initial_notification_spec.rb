# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::PostDeployPipelineInitialNotification do
  subject(:initial_notification) { described_class.new }

  before do
    allow(ReleaseTools::Slack::ChatopsNotification)
      .to receive(:fire_hook)
      .and_return({})
  end

  describe '#no_pending_post_migration_message' do
    it 'sends a chatops message' do
      block_message =
        ":ci_skipped: No pending post migrations available, skipping <https://test.com/-/release-tools/pipeline/123|post-deploy pipeline> execution\n\n"

      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          text: 'No pending post migrations available',
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          blocks: slack_mrkdwn_block(text: block_message)
        )

      pipeline_url = 'https://test.com/-/release-tools/pipeline/123'

      ClimateControl.modify(CI_PIPELINE_URL: pipeline_url) do
        without_dry_run do
          initial_notification.no_pending_post_migrations_message
        end
      end
    end

    context 'with dry-run' do
      it 'skips sending a message' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        initial_notification.no_pending_post_migrations_message
      end
    end
  end

  describe '#production_status_failed_message' do
    let(:production_status) do
      instance_double(
        ReleaseTools::Promotion::ProductionStatus,
        {
          to_slack_blocks: ['blocks']
        }
      )
    end

    it 'sends a chatops message' do
      block_message =
        ":red_circle: <!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}> Post-deployment pipeline is blocked\n\n"

      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          text: 'Post-deployment pipeline is blocked',
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          blocks: slack_mrkdwn_block(text: block_message) + ['blocks']
        )

      without_dry_run do
        initial_notification.production_status_failed_message(production_status)
      end
    end

    context 'with a dry-run' do
      it 'skips sending a message' do
        expect(ReleaseTools::Slack::ChatopsNotification)
          .not_to receive(:fire_hook)

        initial_notification.production_status_failed_message(production_status)
      end
    end
  end
end

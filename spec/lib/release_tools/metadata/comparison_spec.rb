# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metadata::Comparison do
  subject { described_class.new(source: source, target: target) }

  let(:source) { instance_spy(ReleaseTools::ProductVersion) }
  let(:target) { instance_spy(ReleaseTools::ProductVersion) }

  describe '#map_components' do
    it 'yields all projects' do
      allow(subject).to receive(:compare)
      allow(subject).to receive(:source_sha)
      allow(subject).to receive(:target_sha)

      expect { |b| subject.map_components(&b) }
        .to yield_control.exactly(described_class::PROJECTS.length).times
    end
  end

  describe '#compare' do
    let(:project) { ReleaseTools::Project::OmnibusGitlab }

    before do
      allow(source)
        .to receive(:[])
        .with(project.metadata_project_name)
        .and_return(double(sha: 'source_sha'))

      allow(target)
        .to receive(:[])
        .with(project.metadata_project_name)
        .and_return(double(sha: 'target_sha'))
    end

    it 'calls compare API' do
      comparison = instance_double(Gitlab::ObjectifiedHash)
      allow(ReleaseTools::GitlabClient)
        .to receive(:compare)
        .with(project.security_path, from: 'target_sha', to: 'source_sha')
        .and_return(comparison)

      expect(subject.compare(project)).to eq(comparison)
    end
  end

  describe '#source_sha' do
    it 'returns source SHA' do
      project = ReleaseTools::Project::GitlabElasticsearchIndexer
      allow(source)
        .to receive(:[])
        .with(project.metadata_project_name)
        .and_return(double(sha: 'source_sha'))

      expect(subject.source_sha(project)).to eq('source_sha')
    end
  end

  describe '#target_sha' do
    it 'returns target SHA' do
      project = ReleaseTools::Project::GitlabPages
      allow(target)
        .to receive(:[])
        .with(project.metadata_project_name)
        .and_return(double(sha: 'target_sha'))

      expect(subject.target_sha(project)).to eq('target_sha')
    end
  end
end

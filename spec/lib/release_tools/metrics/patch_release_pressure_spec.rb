# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::PatchReleasePressure do
  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  let(:coordinator) { double(:coordinator) }

  let(:patch_pressure) do
    pressure = {
      'severity::1' => 1,
      'severity::2' => 1,
      'severity::3' => 3,
      'severity::4' => 2,
      'none' => 6
    }

    [
      { version: '15.5', pressure: pressure },
      { version: '15.4', pressure: pressure },
      { version: '15.3', pressure: pressure }
    ]
  end

  subject(:patch_release_pressure) { described_class.new }

  before do
    allow(ReleaseTools::Metrics::Client)
      .to receive(:new)
      .and_return(delivery_metrics)

    allow(ReleaseTools::PatchRelease::Coordinator)
      .to receive(:new)
      .and_return(coordinator)

    allow(coordinator)
      .to receive(:pressure_per_severity)
      .and_return(patch_pressure)
  end

  describe '#execute' do
    it 'pushes patch pressure to delivery metrics' do
      expect(delivery_metrics).to receive(:reset).with(described_class::METRIC)

      # -- 15.5
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 1, labels: 'severity::1,15.5')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 1, labels: 'severity::2,15.5')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 3, labels: 'severity::3,15.5')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 2, labels: 'severity::4,15.5')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 6, labels: 'none,15.5')

      # -- 15.4
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 1, labels: 'severity::1,15.4')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 1, labels: 'severity::2,15.4')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 3, labels: 'severity::3,15.4')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 2, labels: 'severity::4,15.4')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 6, labels: 'none,15.4')

      # -- 15.3
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 1, labels: 'severity::1,15.3')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 1, labels: 'severity::2,15.3')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 3, labels: 'severity::3,15.3')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 2, labels: 'severity::4,15.3')

      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 6, labels: 'none,15.3')

      patch_release_pressure.execute
    end
  end
end

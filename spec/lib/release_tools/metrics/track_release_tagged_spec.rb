# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Metrics::TrackReleaseTagged do
  describe '#execute' do
    let(:instance) { described_class.new(version) }
    let(:version) { ReleaseTools::Version.new('1.2.3') }
    let(:fake_metrics) { instance_double(ReleaseTools::Metrics::Client) }

    before do
      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(fake_metrics)
    end

    context 'for monthly release' do
      let(:version) { ReleaseTools::Version.new('1.2.0') }

      it 'increments the package tagging metric for monthly releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", labels: 'monthly,no')

        instance.execute
      end
    end

    context 'for patch release' do
      let(:version) { ReleaseTools::Version.new('1.2.3') }

      it 'increments the package tagging metric for patch releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", labels: 'patch,no')

        instance.execute
      end
    end

    context 'for rc release' do
      let(:version) { ReleaseTools::Version.new('1.2.0-rc1') }

      it 'increments the package tagging metric for rc releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", labels: 'rc,no')

        instance.execute
      end
    end

    context 'for security release' do
      it 'increments the package tagging metric for regular security releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", labels: 'patch,regular')

        ClimateControl.modify(SECURITY: 'true') do
          instance.execute
        end
      end
    end

    context 'for critical security release' do
      it 'increments the package tagging metric for critical security releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", labels: 'patch,critical')

        ClimateControl.modify(SECURITY: 'critical') do
          instance.execute
        end
      end
    end
  end
end

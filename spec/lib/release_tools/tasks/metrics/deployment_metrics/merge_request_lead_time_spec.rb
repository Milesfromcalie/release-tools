# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::Metrics::DeploymentMetrics::MergeRequestLeadTime do
  subject(:service) { described_class.new }

  let(:package_version) { "15.10.202303060320-d244fd30a63.41707614427" }
  let(:metrics) { instance_double(ReleaseTools::Metrics::Client) }
  let(:merged_at) { Time.parse("2023-03-10T12:34:03Z") }
  let(:updated_at) { Time.parse("2023-03-10T12:34:13Z") }

  describe '#execute' do
    before do
      product_version = instance_double(ReleaseTools::ProductVersion)
      allow(ReleaseTools::ProductVersion).to receive(:from_package_version).with(package_version).and_return(product_version)
      allow(product_version).to receive(:metadata).and_return(
        {
          "releases" => {
            ReleaseTools::Project::GitlabEe.metadata_project_name => {
              "sha" => "d244fd30a63"
            }
          }
        }
      )

      deployments = [
        build(
          :deployment,
          id: 12_345,
          environment: double(name: "gprd"),
          sha: "d244fd30a63",
          updated_at: updated_at.iso8601
        )
      ]
      allow(ReleaseTools::GitlabClient).to receive(:deployments)
        .with(ReleaseTools::Project::GitlabEe, "gprd", status: "success").and_return(deployments)

      mr = [
        build(
          :merge_request,
          merged_at: merged_at.iso8601,
          iid: 12_345,
          title: "Mocked title"
        )
      ]

      allow(ReleaseTools::GitlabClient).to receive(:deployed_merge_requests)
        .with(ReleaseTools::Project::GitlabEe, 12_345).and_return(mr)

      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(metrics)
    end

    it 'records MR lead time metric' do
      expect(metrics).to receive(:set)
        .with("deployment_merge_request_lead_time_seconds", 10, labels: "gprd,main,12345,12345,15.10.202303060320-d244fd30a63.41707614427")

      without_dry_run do
        service.execute("gprd", "15.10.202303060320-d244fd30a63.41707614427")
      end
    end

    it 'does not records MR lead time metric during dry_run' do
      expect(metrics).not_to receive(:set)
      service.execute("gprd", "15.10.202303060320-d244fd30a63.41707614427")
    end
  end
end

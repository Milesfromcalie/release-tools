# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::ProductionCheck::Chatops do
  let!(:fake_webhook) do
    stub_const(
      'ReleaseTools::Slack::ChatopsNotification',
      double(fire_hook: true)
    )
  end

  let(:scope) { 'feature_flag' }

  def status_stub(messages = {})
    instance_double(
      ReleaseTools::Promotion::ProductionStatus,
      {
        to_slack_blocks: ['blocks'],
        fine?: true
      }.merge(messages)
    )
  end

  describe '#execute' do
    around do |ex|
      ClimateControl.modify(CHAT_CHANNEL: 'channel', PRODUCTION_CHECK_SCOPE: scope, &ex)
    end

    it 'notifies the specified Slack channel' do
      instance = described_class.new
      status = status_stub(fine?: true)

      stub_const('ReleaseTools::Promotion::ProductionStatus', double(new: status))

      without_dry_run do
        instance.execute
      end

      expect(fake_webhook).to have_received(:fire_hook)
        .with(channel: 'channel', blocks: ['blocks'])
    end

    it 'includes active deployment check by default' do
      instance = described_class.new
      status = class_double(ReleaseTools::Promotion::ProductionStatus)

      expect(status).to receive(:new)
        .with(:canary_up, :active_gprd_deployments, scope: :feature_flag)
        .and_return(spy)

      stub_const('ReleaseTools::Promotion::ProductionStatus', status)

      without_dry_run do
        instance.execute
      end
    end

    it 'skips active deployment check when SKIP_DEPLOYMENT_CHECK is set' do
      instance = described_class.new
      status = class_double(ReleaseTools::Promotion::ProductionStatus)

      expect(status).to receive(:new)
        .with(:canary_up, scope: :feature_flag)
        .and_return(spy)

      stub_const('ReleaseTools::Promotion::ProductionStatus', status)

      ClimateControl.modify(SKIP_DEPLOYMENT_CHECK: 'true') do
        without_dry_run do
          instance.execute
        end
      end
    end

    it 'exits when requested for an unsafe status' do
      instance = described_class.new
      status = status_stub(fine?: false)

      stub_const('ReleaseTools::Promotion::ProductionStatus', double(new: status))

      ClimateControl.modify(FAIL_IF_NOT_SAFE: 'true') do
        expect { instance.execute }.to raise_error(SystemExit) do |error|
          expect(error.status).to eq(10)
        end
      end
    end

    context 'with PRODUCTION_CHECK_SCOPE' do
      let(:status) { class_double(ReleaseTools::Promotion::ProductionStatus) }
      let(:scope) { 'deployment' }

      before do
        allow(status).to receive(:new)
          .with(:canary_up, :active_gprd_deployments, scope: scope.to_sym)
          .and_return(spy)

        stub_const('ReleaseTools::Promotion::ProductionStatus', status)
      end

      it 'calls ProductionStatus with the correct scope' do
        expect(status).to receive(:new)
          .with(:canary_up, :active_gprd_deployments, scope: :deployment)

        without_dry_run do
          described_class.new.execute
        end
      end

      context 'with invalid PRODUCTION_CHECK_SCOPE' do
        let(:scope) { 'invalid' }

        it 'raises UnknownScopeError' do
          without_dry_run do
            expect { described_class.new.execute }.to raise_error(described_class::UnknownScopeError)
          end
        end
      end
    end
  end
end

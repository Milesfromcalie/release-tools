# frozen_string_literal: true

module ReleaseTools
  module Project
    module Quality
      class Base < BaseProject
        def self.requires_qa_issue?
          false
        end

        def self.environment
          raise NotImplementedError
        end
      end
    end
  end
end

# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      class MergeRequestLabeler
        include ::SemanticLogger::Loggable
        include ReleaseTools::Tracker::MergeRequestLabeler

        private

        def deployment_labels
          {
            'db/gstg' => 'workflow::post-deploy-db-staging',
            'db/gprd' => 'workflow::post-deploy-db-production'
          }
        end
      end
    end
  end
end

# frozen_string_literal: true

module ReleaseTools
  module PipelineTracer
    # Helper class for the PipelineTracer module
    class Pipeline
      PIPELINE_URL_REGEX = %r{https://(?<instance>dev\.gitlab\.org|ops\.gitlab\.net|gitlab\.com)/(?<project>.+)/-/pipelines/(?<pipeline_id>\d+)}.freeze

      attr_reader :gitlab_instance, :project, :pipeline_id

      # @param [string] pipeline_url
      def self.from_url(pipeline_url)
        match = PIPELINE_URL_REGEX.match(pipeline_url)

        new(match[:instance], match[:project], match[:pipeline_id])
      end

      # @param [string] gitlab_instance Ex: 'ops.gitlab.net', 'dev.gitlab.org', 'gitlab.com'
      # @param [string] project Ex: 'gitlab-org/release/tools'
      # @param [string, integer] pipeline_id Ex: '12345' or 12345
      def initialize(gitlab_instance, project, pipeline_id)
        @gitlab_instance = gitlab_instance
        @project = project
        @pipeline_id = pipeline_id
      end

      def details
        @details ||=
          Retriable.with_context(:api) do
            client.pipeline(project, pipeline_id)
          end
      end

      def bridge_jobs
        Retriable.with_context(:api) do
          client.pipeline_bridges(project, pipeline_id)
        end
      end

      def jobs
        Retriable.with_context(:api) do
          client.pipeline_jobs(project, pipeline_id, { include_retried: true })
        end
      end

      def url
        "https://#{gitlab_instance}/#{project}/-/pipelines/#{pipeline_id}"
      end

      def start_time
        details.started_at || details.created_at
      end

      def end_time
        details.finished_at || details.updated_at
      end

      def root_attributes
        {
          user: details.user.username,
          web_url: details.web_url,
          created_at: details.created_at,
          updated_at: details.updated_at,
          ref: details.ref,
          duration: details.duration,
          queued_duration: details.queued_duration,
          status: details.status,
          started_at: details.started_at,
          finished_at: details.finished_at
        }
      end

      def client
        @client ||=
          case gitlab_instance
          when 'dev.gitlab.org'
            GitlabDevClient
          when 'ops.gitlab.net'
            GitlabOpsClient
          when 'gitlab.com'
            GitlabClient
          else
            raise "Unknown Gitlab instance: #{gitlab_instance}"
          end
      end
    end
  end
end

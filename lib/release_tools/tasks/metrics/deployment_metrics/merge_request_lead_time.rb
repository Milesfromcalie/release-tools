# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      module DeploymentMetrics
        # Publish metrics MR lead time
        class MergeRequestLeadTime
          def execute(env, version)
            product_version = ProductVersion.from_package_version(version)
            if product_version.metadata.empty?
              ReleaseTools.logger.warn("Product version not found", product_version: product_version.version)
              return nil
            end

            sha = product_version.metadata["releases"][Project::GitlabEe.metadata_project_name]["sha"]

            # We are interesting on successeful deployments only
            deployment = gitlab_client.deployments(Project::GitlabEe, env, status: "success")
                .detect { |dep| dep.sha == sha }

            if deployment.nil?
              ReleaseTools.logger.warn("Deployment not found", product_version: product_version.version)
              return nil
            end

            merge_requests = get_merge_requests(Project::GitlabEe, deployment.id)
            env, stage = parse_env(deployment.environment.name)

            merge_requests.each do |mr|
              lead_time = get_lead_time(deployment.updated_at, mr.merged_at)
              ReleaseTools.logger.info(
                "Recorded MR lead time metric",
                environment: env,
                stage: stage,
                id: mr.iid,
                title: mr.title,
                web_url: mr.web_url,
                version: version
              )

              next if SharedStatus.dry_run?

              metric.set("deployment_merge_request_lead_time_seconds", lead_time, labels: "#{env},#{stage},#{deployment.iid},#{mr.iid},#{version}")
            end
          end

          private

          def gitlab_client
            ReleaseTools::GitlabClient
          end

          def metric
            @metric ||= ReleaseTools::Metrics::Client.new
          end

          def parse_env(environment)
            return [environment, 'main'] unless environment.end_with?('-cny')

            env = environment.delete_suffix('-cny')
            [env, 'cny']
          end

          # This silly method needed to make Rubocop happy about ABC :-[]
          def get_lead_time(updated_at, merged_at)
            Time.parse(updated_at).to_i - Time.parse(merged_at).to_i
          end

          def get_merge_requests(project_id, deployment_id)
            gitlab_client.deployed_merge_requests(project_id, deployment_id)
          end
        end
      end
    end
  end
end
